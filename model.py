#!/usr/bin/python3.4
# -*- coding: utf-8 -*-

from config import database
import mysql.connector as mariadb


class Model:
    def __init__(self):
        """
        Initialize model. Create connect to MariaDB
        """
        self.connection = mariadb.connect(user=database['user'], password=database['password'],
                                          database=database['dbname'])
        self.cursor = self.connection.cursor()

    def add_song(self, uid, link, user):
        """
        Add song to Database and Increment karma to sender
        :param uid: User id
        :param link: Link to song
        :return:
        """
        self.cursor.execute("INSERT INTO song (uid, song) VALUES (%s, %s)", (uid, link))
        self.connection.commit()
        karma = self.karma_status(uid)
        if karma == None:
            self.cursor.execute("INSERT INTO karma (uid, karma_rate, username) VALUES (%s, 1, %s)",
                                (uid, ' '.join([user.first_name, user.last_name])))
        else:
            self.cursor.execute("UPDATE karma SET karma_rate = karma_rate + 1 WHERE uid = %s", (uid,))
        self.connection.commit()

    def karma_status(self, uid):
        """
        Return karma rate for selected user
        :param uid: User id
        :return: Cursor with karma_rate
        """
        self.cursor.execute("SELECT karma_rate FROM karma WHERE uid = %s", (uid,))
        return self.cursor.fetchone()

    def get_random_song(self):
        """
        Get Random song from Database
        :return: Cursor with Random Song
        """
        self.cursor.execute("SELECT song FROM song ORDER BY RAND() LIMIT 1")
        return self.cursor.fetchone()

    def karma_rate(self):
        """
        Get Top-3 People by Karma
        :return:
        """
        self.cursor.execute("SELECT username, karma_rate FROM karma ORDER BY karma_rate DESC LIMIT 3")
        return self.cursor.fetchall()
