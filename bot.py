#!/usr/bin/python3.4
# -*- coding: utf-8 -*-

import telebot
import cherrypy
import config
from model import Model

WEBHOOK_HOST = '95.213.237.55'
WEBHOOK_POST = 8443
WEBHOOK_LISTEN = '95.213.237.55'
WEBHOOK_SSL_CERT = 'webhook_cert.pem'
WEBHOOK_SSL_PRIV = 'webhook_pkey.pem'
WEBHOOK_URL_BASE = "https://%s:%s" % (WEBHOOK_HOST, WEBHOOK_POST)
WEBHOOK_URL_PATH = "/%s/" % config.token

bot = telebot.TeleBot(config.token)
model = Model()


class WebhookServer(object):
    @cherrypy.expose
    def index(self):
        """
        Check request from Webhook
        :return:
        """
        if 'content-length' in cherrypy.request.headers and \
                        'content-type' in cherrypy.request.headers and \
                        cherrypy.request.headers['content-type'] == 'application/json':
            length = int(cherrypy.request.headers['content-length'])
            json_string = cherrypy.request.body.read(length).decode('utf-8')
            update = telebot.types.Update.de_json(json_string)
            bot.process_new_updates([update])
            return ''
        else:
            raise cherrypy.HTTPError(403)


@bot.message_handler(commands=['add_song'])
def add_song(message):
    """
    Add song from user to Database
    :param message:
    :return:
    """
    replace_string = '/add_song@{0}'.format(config.bot_name)
    text = message.text.replace(replace_string, '') # Remove command for group chat
    text = message.text.replace('/add_song', '') # Remove command for single chat
    text = text.strip()
    if text != '':
        model.add_song(message.from_user.id, text, message.from_user)
        bot.reply_to(message, '{0} {1} added a song!'.format(message.from_user.first_name, message.from_user.last_name))


@bot.message_handler(commands=['random'])
def random(message):
    """
    Send to chat random song from Database
    :param message:
    :return:
    """
    data = model.get_random_song()
    bot.reply_to(message, data[0])


@bot.message_handler(commands=['karma'])
def karma(message):
    """
    Return karma for user who send message
    :param message:
    :return:
    """
    data = model.karma_status(message.from_user.id)
    bot.reply_to(message,
                 '{0} {1}: {2}'.format(message.from_user.first_name, message.from_user.last_name, data[0]))


@bot.message_handler(commands=['karma_rate'])
def karma_rate(message):
    data = model.karma_rate()
    karma_strings = []
    for item in data:
        karma_strings.append('{0} - {1} karma'.format(item[0], item[1]))
    bot.reply_to(message, "\n".join(karma_strings))


bot.remove_webhook()
bot.set_webhook(url=WEBHOOK_URL_BASE + WEBHOOK_URL_PATH, certificate=open(WEBHOOK_SSL_CERT, 'r'))

cherrypy.config.update({
    'server.socket_host': WEBHOOK_LISTEN,
    'server.socket_port': WEBHOOK_POST,
    'server.ssl_module': 'builtin',
    'server.ssl_certificate': WEBHOOK_SSL_CERT,
    'server.ssl_private_key': WEBHOOK_SSL_PRIV
})
cherrypy.quickstart(WebhookServer(), WEBHOOK_URL_PATH, {'/': {}})
